﻿using System.Collections.Generic;
using System.Linq;

namespace Grafos01
{
    class Vertex
    {
        private int id;
        private List<Edge> edges;

        public Vertex(int Id)
        {
            this.id = Id;
            edges = new List<Edge>();
        }
        
        public int getID()
        {
            return this.id;
        }
        public void addEdge(Edge edge)
        {
            if (!edges.Contains(edge))
                edges.Add(edge);
        }

        public void removeEdges()
        {     
            while (edges.Count() > 0)
            {
                removeEdge(edges[0]);                
            }                   
        }

        public void removeEdgeVi(Vertex vertexVj)
        {
            Edge edge = edges.Where(p => p.getVi() == this && p.getVj() == vertexVj).FirstOrDefault();
            if (edge != null)
                removeEdge(edge);
        }

        public void removeEdge(Edge edge)
        {
            if (edges.Contains(edge))
            {
                edges.Remove(edge);
                if (edge.getVi() == this)
                    edge.getVj().removeEdge(edge);
                else
                    edge.getVi().removeEdge(edge);
            }
        }

        public bool isAdjacent(int id_vj)
        {
            return edges.Where(p => p.getVjID() == id_vj).Count() > 0;
        }

        public List<Vertex> adj()
        {
            List<Vertex> vertexes = new List<Vertex>();
            foreach (Edge edge in edges)
            {
                if (edge.getVi() == this)
                    vertexes.Add(edge.getVj());
                else
                    vertexes.Add(edge.getVi());
            }

            return vertexes;
        }

        public int E()
        {
            return edges.Count();
        }
    }
}
