﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafos01
{
    class DepthFirstSearch
    {
        List<TreeDepth> treeDepth;
        Graph Grafo;
        long Time;
        public DepthFirstSearch(Graph Grafo)
        {
            treeDepth = new List<TreeDepth>();
            this.Grafo = Grafo; 
        }

        public void CreateTree()
        {
            Time = 0;
            //Carregar Lista de vetores
            foreach (Vertex v in Grafo.getListVertex())
            {
                TreeDepth item = new TreeDepth(v);
                treeDepth.Add(item);                
            }

            foreach (TreeDepth item in treeDepth)
            {
                if (item.Color == 'w')
                    DFSVisit(item);
            }
        }


        private void DFSVisit(TreeDepth Item)
        {
            Item.Color = 'g';
            Time++;
            Item.TimeA = Time;

            foreach(Vertex v in Item.V.adj())
            {
                TreeDepth itemAdj = treeDepth.Where(p => p.V == v).FirstOrDefault();
                if (itemAdj.Color == 'w')
                {
                    itemAdj.Adj = Item.V;
                    DFSVisit(itemAdj);
                }
            }
            Item.Color = 'b';
            Time++;
            Item.TimeB = Time;
        }

        public override string ToString()
        {
            StringBuilder arvore = new StringBuilder();
            foreach (TreeDepth item in treeDepth)
            {
                arvore.Append(string.Format())
            }

            return arvore.ToString();
        }


        #region estrutura
        class TreeDepth
        {
            public Vertex V;
            public char Color;
            public Vertex Adj;
            public long TimeA;
            public long TimeB;

            public TreeDepth(Vertex v)
            {
                V = v;
                Color = 'w';
                Adj = null;
                TimeA = 0;
                TimeB = 0;
            }
        }
        #endregion
    }
}
