﻿using System;

namespace Grafos01
{
    class Teste
    {
        public void TesteGrafo01()
        {
            Graph grafo = new Graph();
            //Criar Vertices
            grafo.addVertex(1);
            grafo.addVertex(2);
            grafo.addVertex(3);
            grafo.addVertex(4);

            //Criar arestas
            //Vertice 01
            grafo.addEdge(1, 2);
            grafo.addEdge(1, 3);
            grafo.addEdge(1, 4);
            grafo.addEdge(1, 4);
            
            //vertice 02
            grafo.addEdge(2, 3);
            //vertice 03
            grafo.addEdge(3, 3);

            Console.WriteLine("Ordem: {0} \r\nTamanho: {1}", grafo.V(), grafo.E());
            Console.WriteLine(grafo.ToString());

            for (int i = 1; i <= 4; i++)
            {
                grafo.removeVertex(i);
                Console.WriteLine("Ordem: {0} \r\nTamanho: {1}", grafo.V(), grafo.E());
                Console.WriteLine(grafo.ToString());                
            }
            Console.ReadKey();
        }
    }
}
