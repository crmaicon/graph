﻿namespace Grafos01
{
    class Edge
    {
        private Vertex vi;
        private Vertex vj;

        public Edge(Vertex Vi, Vertex Vj)
        {
            this.vi = Vi;
            this.vj = Vj;
        }

        public Vertex getVi()
        {
            return vi;
        }

        public Vertex getVj()
        {
            return vj;
        }

        public int getViID()
        {
            return vi.getID();
        }
        public int getVjID()
        {
            return vj.getID();
        }

        public string toString()
        {
            return string.Format("Vi: {0} - Vj: {1}", vi.getID(), vj.getID());
        }
    }
}
