﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Grafos01
{
    class Graph
    {
        private List<Vertex> vertexes;
        
        public Graph()
        {
            vertexes = new List<Vertex>();
        }

        public List<Vertex> getListVertex()
        {
            return vertexes;
        }

        public bool addVertex(int Id)
        {
            try
            {
                if (vertexes.Where(p => p.getID() == Id).FirstOrDefault() == null)
                {
                    Vertex vertex = new Vertex(Id);
                    vertexes.Add(vertex);
                }
                else
                    return false;
            }
            catch(Exception err)
            {
                return false;
            }
            return true;
        }
        public bool removeVertex(int Id)
        {
            try
            {
                Vertex vertex = vertexes.Where(p => p.getID() == Id).FirstOrDefault();
                if (vertex != null)
                    vertex.removeEdges();
                vertexes = vertexes.Where(p => p.getID() != Id).ToList();
            }
            catch(Exception err)
            {
                return false;
            }
            return true;
        }
        public bool addEdge(int Id_vi, int Id_vj)
        {
            try
            {
                Vertex vi = vertexes.Where(p => p.getID() == Id_vi).FirstOrDefault();
                Vertex vj = vertexes.Where(p => p.getID() == Id_vj).FirstOrDefault();
                if (vi != null && vj != null)
                {
                    Edge edge = new Edge(vi, vj);
                    vi.addEdge(edge);
                    vj.addEdge(edge);
                }
                else
                {
                    return false;
                }
            }
            catch(Exception err)
            {
                return false;
            }
            return true;
        }
        public bool removeEdge(int Id_vi, int Id_vj)
        {
            try
            {
                Vertex vi = vertexes.Where(p => p.getID() == Id_vi).FirstOrDefault();
                Vertex vj = vertexes.Where(p => p.getID() == Id_vj).FirstOrDefault();
                if (vi != null && vj != null)
                {
                    vi.removeEdgeVi(vj);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                return false;
            }
            return true;
        }
        public int V()
        {
            return vertexes.Count();
        }
        public int E()
        {
            return vertexes.Sum(p => p.E());
        }

        public override string ToString()
        {
            string grafo = "";
            foreach(Vertex vertex in vertexes)
            {
                string vertice = string.Format("{0}:", vertex.getID());
                foreach(Vertex vertexAdj in vertex.adj())
                {
                    vertice += string.Format(" {0}", vertexAdj.getID());
                }
                grafo += string.Format("{0} \r\n", vertice);
            }
            return grafo;
        }        
    }
}
